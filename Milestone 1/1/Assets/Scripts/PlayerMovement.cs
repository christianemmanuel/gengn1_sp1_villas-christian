﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    Rigidbody2D body;
    float horizontal;
    float vertical;
    float moveLimiter = 0.7f;
    float runSpeed;

    Camera cam;
    Transform my;

    public bool IsStopped;

    void Start()
    {
        cam = Camera.main;
        my = GetComponent<Transform>();
        body = GetComponent<Rigidbody2D>();
        IsStopped = true;
        runSpeed = 10;

    }

    void Update()
    {
        if (body.velocity.x != 0 || body.velocity.y != 0)
        {
            IsStopped = false;
        }
        else
        {
            IsStopped = true;
        }


        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");
    }

    void FixedUpdate()
    {
        if (horizontal != 0 && vertical != 0)
        {
            body.velocity = new Vector2((horizontal * runSpeed) * moveLimiter, (vertical * runSpeed) * moveLimiter);

        }
        else
        {
            body.velocity = new Vector2(horizontal * runSpeed, vertical * runSpeed);

        }


    }

    public void StopMovement()
    {
        body.velocity = Vector2.zero;
        IsStopped = true;
    }

}

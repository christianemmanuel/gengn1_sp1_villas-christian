﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMovement : MonoBehaviour
{
    public float MovementRadius;


    public bool IsMoving;

    public float speed = 3f;

    public Vector3 FirstWaypoint;
    bool atFirst;
    bool assignedMovement;

    public float TimetilNextWaypoint;
    float timer;

    Vector3 nextWaypoint;

    float minX, maxX;
    float minY, maxY;


    void Clamp()
    {
        transform.position = new Vector3
       (
           Mathf.Clamp(transform.position.x, minX, maxX),
           Mathf.Clamp(transform.position.y, minY, maxY),
           transform.position.z
       );
    }

    void Start()
    {
        atFirst = false;
        assignedMovement = false;
        timer = TimetilNextWaypoint;       
    }

    // Update is called once per frame
    void Update()
    {
        StartMovement();
    }
    
    void StartMovement()
    {

        timer -= Time.deltaTime;
        if (timer <= 0f)
        {
            nextWaypoint = RandominRadius();
            LookatWaypoint(nextWaypoint);
            assignedMovement = true;

            timer = TimetilNextWaypoint;
        }

        if (assignedMovement)
        {
            Moveto(nextWaypoint);
        }

    }

    Vector3 RandominRadius()
    {
        Vector3 v2 = Random.insideUnitCircle * MovementRadius;

        return v2;
    }


    void LookatWaypoint(Vector3 waypointpos)
    {
        transform.right = waypointpos - transform.position;
    }

    void Moveto(Vector3 waypointpos)
    {

        Vector3 currentPosition = transform.position;

        if (Vector3.Distance(currentPosition, waypointpos) > .1f)
        {

            Vector3 directionOfTravel = waypointpos - currentPosition;
            directionOfTravel.Normalize();

            this.transform.Translate(
                directionOfTravel.x * speed * Time.deltaTime,
                directionOfTravel.y * speed * Time.deltaTime,
                directionOfTravel.z * speed * Time.deltaTime,
                Space.World
            );
        }
        else
        {
            if (!atFirst)
            {
                atFirst = true;
            }

        }
    }

    public void AssignFirstWaypoint(Vector3 wayp)
    {
        FirstWaypoint = wayp;
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, MovementRadius);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndScreen : MonoBehaviour
{

    public Text Score;
    int scoreflt;

	// Use this for initialization
	void Start () {
        scoreflt = GameObject.Find("Table").GetComponent<Table>().scoreNum;
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>().enabled = false;
    }
	
	// Update is called once per frame
	void Update ()
    {
        Score.text = "Final Score -- " + scoreflt;

    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void End()
    {
        Application.Quit();
    }
}

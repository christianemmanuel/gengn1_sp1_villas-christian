﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Table : MonoBehaviour {

    public Text ScoreText;
    public int scoreNum;

    public GameObject holder;

    public Text TimeLeft;
    public float timer;

    public GameObject EndScreen;
    

	// Use this for initialization
	void Start () {
        scoreNum = 0;
        EndScreen.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        ScoreText.text = "Score -- " + scoreNum;
        TimeLeft.text = "Time Left -- " + timer.ToString("00");
        if (timer < 0)
        {
            Ended();
        }
        else
        {
            timer -= Time.deltaTime;
        }
    }

    void Ended()
    {
        EndScreen.SetActive(true);
    }

    void OnTriggerEnter2D(Collider2D Col)
    {
        if (Col.tag == "Player")
        {
            if (Col.gameObject.GetComponent<PlayerCarry>().Carried != null)
            {
                Col.gameObject.GetComponent<PlayerCarry>().Carried.GetComponent<AIWound>().Moveto(holder);
                Col.gameObject.GetComponent<PlayerCarry>().Drop();
                
                scoreNum += 10;
            }
        }
    }

  
}

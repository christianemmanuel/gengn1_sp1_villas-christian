﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCarry : MonoBehaviour {

    public bool canCarry;
    public GameObject Carried;

	// Use this for initialization
	void Start () {
        canCarry = true;
        
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerStay2D(Collider2D col)
    {        
        if (col.tag == "NPC")
        {
            if (col.gameObject.GetComponent<AIWound>().isWounded)
            {
                if(canCarry)
                {
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        Carry(col.gameObject);
                        
                    }
                }
                else
                {
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        Drop();
                    }
                }
            }
        }
    }

    void Carry(GameObject g)
    {
        canCarry = false;
        Carried = g;
        Debug.Log("Carry");
        g.GetComponent<AIWound>().Carry(gameObject);

    }

    public void Drop()
    {
        Carried.GetComponent<AIWound>().Drop();
        Carried = null;
        canCarry = true;
    }

    

}

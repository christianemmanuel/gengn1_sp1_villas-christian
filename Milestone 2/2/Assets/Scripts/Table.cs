﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Table : MonoBehaviour {

    public Text ScoreText;
    int scoreNum;

    public GameObject holder;
    

	// Use this for initialization
	void Start () {
        scoreNum = 0;

    }
	
	// Update is called once per frame
	void Update () {
        ScoreText.text = "Score -- " + scoreNum;

    }

    void OnTriggerStay2D(Collider2D col)
    {
        if(Input.GetKeyDown(KeyCode.E))
        {
            if (!col.gameObject.GetComponent<PlayerCarry>().canCarry)
            {
                col.gameObject.GetComponent<PlayerCarry>().Drop();
                col.gameObject.GetComponent<PlayerCarry>().Carried.GetComponent<AIWound>().Moveto(holder);
                scoreNum += 10;
            }
        }
       
    }
}

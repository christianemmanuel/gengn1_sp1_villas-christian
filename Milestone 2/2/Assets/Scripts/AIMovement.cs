﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMovement : MonoBehaviour {
    public float MovementRadius;


    public bool IsMoving;

    public float speed = 3f;

    public Vector3 FirstWaypoint;
    bool atFirst;
    bool assignedMovement;

    public float TimetilNextWaypoint;
    float timer;

    Vector3 nextWaypoint;

    float minX, maxX;
    float minY, maxY;

   // GameManager GM;

    void Clamp()
    {
        transform.position = new Vector3
       (
           Mathf.Clamp(transform.position.x, minX, maxX),
           Mathf.Clamp(transform.position.y, minY, maxY),
           transform.position.z
       );
    }

    // Use this for initialization
    void Start()
    {
        //GM = GameObject.Find("GameManager").GetComponent<GameManager>();

        atFirst = false;
        assignedMovement = false;
        timer = TimetilNextWaypoint;       
    }

    // Update is called once per frame
    void Update()
    {
        //if (!GetComponent<FriendlyStats>().IsWounded && !GetComponent<FriendlyStats>().IsDead)
        //{
        //    switch (atFirst)
        //    {
        //        case true:
        //            StartMovement();
        //            break;

        //        case false:
        //            LookatWaypoint(FirstWaypoint);
        //            Moveto(FirstWaypoint);
        //            break;
        //    }
        //}

        StartMovement();
    }
    
    void StartMovement()
    {

        timer -= Time.deltaTime;
        if (timer <= 0f)
        {
            nextWaypoint = RandominRadius();
            LookatWaypoint(nextWaypoint);
            assignedMovement = true;

            timer = TimetilNextWaypoint;
        }

        if (assignedMovement)
        {
            Moveto(nextWaypoint);
        }
        //Clamp();
    }

    Vector3 RandominRadius()
    {
        Vector3 v2 = Random.insideUnitCircle * MovementRadius;

        //Vector3 v3 = new Vector3(v2.x,v2.y,0);
        return v2;
    }


    void LookatWaypoint(Vector3 waypointpos)
    {
        transform.right = waypointpos - transform.position;
    }

    void Moveto(Vector3 waypointpos)
    {

        Vector3 currentPosition = transform.position;

        if (Vector3.Distance(currentPosition, waypointpos) > .1f)
        {

            Vector3 directionOfTravel = waypointpos - currentPosition;
            directionOfTravel.Normalize();

            //Debug.Log("Moveto -- " + directionOfTravel);

            this.transform.Translate(
                directionOfTravel.x * speed * Time.deltaTime,
                directionOfTravel.y * speed * Time.deltaTime,
                directionOfTravel.z * speed * Time.deltaTime,
                Space.World
            );
        }
        else
        {
            if (!atFirst)
            {
                atFirst = true;
            }

        }
    }

    public void AssignFirstWaypoint(Vector3 wayp)
    {
        FirstWaypoint = wayp;
    }

    void OnDrawGizmosSelected()
    {
        // Display the explosion radius when selected
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, MovementRadius);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIWound : MonoBehaviour {

    float timer;
    public bool isWounded;

    public bool BeingCarried;

    public GameObject Player;

	// Use this for initialization
	void Start () {
        timer = 0;
        BeingCarried = false;
    }
	
	// Update is called once per frame
	void Update () {
       
        //Debug.Log(timer);
        if(timer > 10)
        {
            isWounded = true;
        }
        else
        {
            timer += Time.deltaTime;
        }

        if (isWounded)
        {
            GetComponent<SpriteRenderer>().color = Color.red;
            GetComponent<AIMovement>().enabled = false;

        }    
        
        if (BeingCarried)
        {
            transform.position = Player.transform.position;
        }

	}

    public void Carry(GameObject g)
    {
        Player = g;
        BeingCarried = true;
    }

    public void Drop()
    {
        BeingCarried = false;
    }

    public void Moveto(GameObject g)
    {
        transform.position = g.transform.position;
    }
}
